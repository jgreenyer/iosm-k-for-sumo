package isomk4sumo

import de.tudresden.sumo.cmd.*
import de.tudresden.ws.container.SumoPosition2D
import de.tudresden.ws.container.SumoStringList
import it.polito.appeal.traci.SumoTraciConnection
import org.scenariotools.iosmk.MessageEvent
import kotlin.reflect.KFunction


abstract class SumoSimulationElement(val sumoSimulation:SumoSimulation, val sumoTraciConnection:SumoTraciConnection)


class SumoSimulation(val sumoTraciConnection:SumoTraciConnection){

    var closed = true

    fun runServer(){
        sumoTraciConnection.runServer()
        closed = false
    }

    fun close(){
        sumoTraciConnection.close()
        closed = true
    }

    fun do_timestep(){
        sumoTraciConnection.do_timestep()
    }

    fun getMinExpectedNumber() : Int {
        return sumoTraciConnection.do_job_get(Simulation.getMinExpectedNumber()) as Int
    }

    fun getLoadedIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Simulation.getLoadedIDList()) as SumoStringList).toTypedArray()
    }

    fun getDepartedIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Simulation.getDepartedIDList()) as SumoStringList).toTypedArray()
    }

    fun getArrivedIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Simulation.getArrivedIDList()) as SumoStringList).toTypedArray()
    }

    fun vehicleAppeared(id:String) : SumoVehicle{
        val newVehicle = SumoVehicle(this, id)
        vehicles.put(id, newVehicle)
        return newVehicle
    }

    fun vehicleDisAppeared(id:String) : SumoVehicle?{
        return vehicles.remove(id)
    }

    val vehicles : MutableMap<String,SumoVehicle> = mutableMapOf()

    fun vehiclesUpdated() {}

    fun roadNetworkCreated() {}

    val edges : MutableMap<String,SumoEdge> = mutableMapOf()
    val lanes : MutableMap<String,SumoLane> = mutableMapOf()
    val junctions : MutableMap<String,SumoJunction> = mutableMapOf()
    val persons : MutableMap<String,SumoPerson> = mutableMapOf()

    fun getPersonsIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Person.getIDList()) as SumoStringList).toTypedArray()
    }

    fun personAppeared(id:String) : SumoPerson{
        val newPerson = SumoPerson(this, id)
        persons.put(id, newPerson)
        return newPerson
    }

    fun personDisAppeared(id:String) : SumoPerson?{
        return persons.remove(id)
    }

    fun personsUpdated() {}

    val trafficLights : MutableMap<String,SumoTrafficLight> = mutableMapOf()

    fun getTrafficLightIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Trafficlight.getIDList()) as SumoStringList).toTypedArray()
    }

    fun getEdgeIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Edge.getIDList()) as SumoStringList).toTypedArray()
    }

    fun getLaneIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Lane.getIDList()) as SumoStringList).toTypedArray()
    }

    fun getJunctionIDList() : Array<String>{
        return (sumoTraciConnection.do_job_get(Junction.getIDList()) as SumoStringList).toTypedArray()
    }

    fun getLaneEdgeID(laneID:String) : String{
        return (sumoTraciConnection.do_job_get(Lane.getEdgeID(laneID)) as String)
    }

    val environment = Environment()

}

class SumoVehicle(sumoSimulation: SumoSimulation, val id:String, var lane:SumoLane? = null, val position : Position2D = Position2D(0.0, 0.0), var speed : Double = 0.0) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun refreshPosition() : Boolean {
        val sumoPosition2D = sumoTraciConnection.do_job_get(Vehicle.getPosition(id)) as SumoPosition2D

        if (!position.equalsSumoPosition2D(sumoPosition2D)){
            position.setToSumoPosition2D(sumoPosition2D)
            return true
        }else
            return false
    }

    fun positionChanged(position : Position2D){}

    fun speedChanged(speed : Double){}

    fun refreshedSpeed() : Boolean {
        val sumoVehicleSpeed = sumoTraciConnection.do_job_get(Vehicle.getSpeed(id)) as Double

        if (!speed.equals(sumoVehicleSpeed)){
            speed = sumoVehicleSpeed
            return true
        }else
            return false
    }

    fun refreshLane() : Boolean {
        val laneID = sumoTraciConnection.do_job_get(Vehicle.getLaneID(id)) as String
        val currentLane = sumoSimulation.lanes.get(laneID)
        if (currentLane != lane){
            println("Vehicle $id lane changed: now on lane $laneID")
            lane = currentLane
            return true
        } else
            return false
    }

    val vehicleController = VehicleController(this)

}


class SumoJunction(sumoSimulation: SumoSimulation, val id:String, val position : Position2D = Position2D(0.0, 0.0)) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun retrievePosition() : Position2D{
        val sumoPosition2D= sumoTraciConnection.do_job_get(Junction.getPosition(id)) as SumoPosition2D
        position.setToSumoPosition2D(sumoPosition2D)
        return position
    }
}

class SumoEdge(sumoSimulation: SumoSimulation, val id:String) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun getLastStepPersonIDs() : Array<String>{
        return (sumoTraciConnection.do_job_get(Edge.getLastStepPersonIDs(id)) as SumoStringList).toTypedArray()
    }

}

class SumoLane(sumoSimulation: SumoSimulation, val id:String, val edge:SumoEdge) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    val laneController = LaneController(this)

}

class SumoPerson(sumoSimulation: SumoSimulation, val id:String, var edge:SumoEdge? = null) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun changeEdge(newEdge : SumoEdge) {
        println("Person $id changed to edge ${newEdge.id}")
        edge = newEdge
    }

}

class SumoTrafficLight(sumoSimulation: SumoSimulation, val id:String, var currentPhase : Int = 0) : SumoSimulationElement(sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun refreshPhase() : Boolean{
        val newCurrentPhase = sumoTraciConnection.do_job_get(Trafficlight.getPhase(id)) as Int
        if (newCurrentPhase == currentPhase){
            return false
        } else  {
            currentPhase = newCurrentPhase
            return true
        }
    }

    fun setPhase(phase : Int){
        sumoTraciConnection.do_job_set(Trafficlight.setPhase(id,phase))
    }

    val trafficLightController = TrafficLightController(this)

}


data class Position2D(var x:Double, var y:Double){

    fun equalsSumoPosition2D(sumoPosition2D:SumoPosition2D) = (x==sumoPosition2D.x && y==sumoPosition2D.y)
    fun setToSumoPosition2D(sumoPosition2D:SumoPosition2D) {
        x=sumoPosition2D.x
        y=sumoPosition2D.y
    }
    fun distance(x:Double, y:Double) = Math.hypot(this.x-x, this.y-y)
    fun distance(position2D : Position2D) = distance(position2D.x, position2D.y)

    fun copy() = Position2D(this.x, this.y)

}