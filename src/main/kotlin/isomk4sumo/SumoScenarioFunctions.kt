package isomk4sumo

import org.scenariotools.bpk.IEventSet
import org.scenariotools.bpk.MutableNonConcreteEventSet
import org.scenariotools.iosmk.*


val timeStepEvent = SymbolicMessageEvent(SumoSimulation::do_timestep)

@Suppress("UNCHECKED_CAST")
suspend fun <T> Scenario.urgent(messageEvent: MessageEvent<T>) : MessageEvent<T>{
    return requestAndBlock(messageEvent, timeStepEvent) as MessageEvent<T>
}

@Suppress("UNCHECKED_CAST")
suspend fun <T> Scenario.urgentAndBlock(messageEvent: MessageEvent<T>, blockedEvents : IEventSet) : MessageEvent<T>{
    val allBlockedEvents = MutableNonConcreteEventSet()
    allBlockedEvents.add(timeStepEvent)
    allBlockedEvents.add(blockedEvents)
    return requestAndBlock(messageEvent, allBlockedEvents) as MessageEvent<T>
}


suspend fun Scenario.waitForSimulationSteps(numberOfSteps : Int){
    for (i in 0 .. numberOfSteps) {
        waitFor(timeStepEvent)
    }
}

fun Scenario.getSumoSimulationFromLastMessageEvent() : SumoSimulation?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is SumoSimulationElement)
            return receiver.sumoSimulation
        else if (receiver is SumoSimulation)
            return receiver
    }
    return null
}

fun Scenario.getSumoPersonFromLastMessageEvent() : SumoPerson?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is SumoPerson) return receiver
    }
    return null
}

fun Scenario.getSumoTrafficLightFromLastMessageEvent() : SumoTrafficLight?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is SumoTrafficLight) return receiver
    }
    return null
}

fun Scenario.getSumoVehicleFromLastMessageEvent() : SumoVehicle?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is SumoVehicle) return receiver
        val sender = it.sender
        if (sender is SumoVehicle) return sender
    }
    return null
}

fun Scenario.getVehicleControllerFromLastMessageEvent() : VehicleController?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is VehicleController) return receiver
        val sender = it.sender
        if (sender is VehicleController) return sender
    }
    return null
}

fun Scenario.getTrafficLightControllerFromLastMessageEvent() : TrafficLightController?{
    lastMessage()?.let {
        val receiver = it.receiver
        if (receiver is TrafficLightController) return receiver
    }
    return null
}

suspend inline fun Scenario.expect(condition : ()->Boolean, afterNumberOfSteps: Int, message:String = ""){
    var counter = 0
    while (counter < afterNumberOfSteps){
        if (condition()) return
        waitFor(SumoSimulation::do_timestep.symbolicEvent())
        counter++
    }
    throw ViolationException(message)
}
