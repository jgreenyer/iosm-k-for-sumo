package isomk4sumo

import it.polito.appeal.traci.SumoTraciConnection
import org.scenariotools.iosmk.*


fun createSumoScenarioProgram(sumo_bin:String, config_file:String, step_length_sec:Double) : ScenarioProgram{

    val sumoTraciConnection = SumoTraciConnection(sumo_bin, config_file)

    val sumoSimulation = SumoSimulation(sumoTraciConnection)

    sumoTraciConnection.addOption("step-length", step_length_sec.toString())

    val runSumo = scenario("RunSumo") {
        try {

            request(sumoSimulation sendSelf SumoSimulation::runServer)

            do {
                request(sumoSimulation sendSelf SumoSimulation::do_timestep)
                val minExpectedNumberOfVehicles = request(sumoSimulation sendSelf SumoSimulation::getMinExpectedNumber).result()!!
            } while (minExpectedNumberOfVehicles > 0)

            request(sumoSimulation sendSelf SumoSimulation::close)

        } catch (ex: Exception) {
            println("Exception occurred...")
            ex.printStackTrace()
        }

    }

    val scenarioProgram = ScenarioProgram()
    scenarioProgram.environmentClasses.addAll(
            setOf(SumoSimulationElement::class, Environment::class)
    )

    scenarioProgram.addAssumptionScenario(runSumo)

    return scenarioProgram
}



