package isomk4sumo

import org.scenariotools.iosmk.MessageEvent

class VehicleController(val sumoVehicle: SumoVehicle){

    fun inTrafficLightPerimeter(trafficLightController: TrafficLightController){}
    fun passedTrafficLight(trafficLightController: TrafficLightController){}


}

class TrafficLightController(val sumoTrafficLight: SumoTrafficLight){

    fun vehicleApproaching(){}

    fun pedestrianApproachingCrossing(pedestrianID : String){}

    fun allPedestriansLeftTriggerAreas(){}

    val detectedPedestrianIDs = mutableSetOf<String>()

    fun addDetectedPedestrianIDs(pedestrianID : String){
        detectedPedestrianIDs.add(pedestrianID)
    }
    fun removeDetectedPedestrianIDs(pedestrianID : String){
        detectedPedestrianIDs.remove(pedestrianID)
    }

    fun transitionToPhase(phase : Int){}

    fun phaseChanged(phase : Int){}

    var currentPhaseActiveSeconds = 0
    fun currentPhasedIsActiveFor(seconds : Int){
        currentPhaseActiveSeconds = seconds
    }

}

class Environment(val name:String = "Env")

class LaneController(val sumoLanes : SumoLane){

}