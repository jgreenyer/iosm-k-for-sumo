package isomk4sumo.activescenarios

import isomk4sumo.*
import isomk4sumo.getSumoSimulationFromLastMessageEvent
import isomk4sumo.urgent
import org.scenariotools.iosmk.*

val personsMaintainer = scenario("PersonsMaintainer"){

    val personsIDList = mutableSetOf<String>()

    do {

        waitFor(SumoSimulation::do_timestep.symbolicEvent())
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        val newPersonsIDList = urgent(sumoSimulation sendSelf SumoSimulation::getPersonsIDList).result()!!

        val appearedPersonsIDList = newPersonsIDList.toMutableSet().also{it.removeAll(personsIDList)}
        for (id in appearedPersonsIDList){
            urgent(sumoSimulation sendSelf SumoSimulation::personAppeared param id)
        }

        val disappearedPersonsIDList = personsIDList.toMutableSet().also{it.removeAll(newPersonsIDList)}
        for (id in disappearedPersonsIDList){
            urgent(sumoSimulation sendSelf SumoSimulation::personDisAppeared param id)
        }

        personsIDList.clear()
        personsIDList.addAll(newPersonsIDList)

        urgent(sumoSimulation sendSelf SumoSimulation::personsUpdated)

    } while (!sumoSimulation.closed)
}
