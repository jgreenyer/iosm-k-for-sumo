package isomk4sumo.activescenarios

import isomk4sumo.*
import org.scenariotools.iosmk.SymbolicMessageEvent
import org.scenariotools.iosmk.scenario
import org.scenariotools.iosmk.sendSelf

val vehicleLanesMaintainer = scenario("VehicleLanesMaintainer") {

    do {
        waitFor(SymbolicMessageEvent(SumoSimulation::vehiclesUpdated))
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        for (sumoVehicle in sumoSimulation.vehicles.values.toTypedArray()){ //using .toTypedArray() in order to avoid ConcurrentModificationException
            urgent(sumoVehicle sendSelf SumoVehicle::refreshLane)
        }

    } while (!sumoSimulation.closed)
}
