package isomk4sumo.activescenarios

import isomk4sumo.*
import org.scenariotools.iosmk.*

val vehiclePositionsMaintainer = scenario("VehiclePositionsMaintainer") {
   do {
        waitFor(SymbolicMessageEvent(SumoSimulation::vehiclesUpdated))
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        for (sumoVehicle in sumoSimulation.vehicles.values){
            if (urgent(sumoVehicle sendSelf SumoVehicle::refreshPosition).result()!!)
                urgent(sumoVehicle sendSelf SumoVehicle::positionChanged param sumoVehicle.position)
        }

    } while (!sumoSimulation.closed)
}
