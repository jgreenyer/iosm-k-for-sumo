package isomk4sumo.activescenarios

import isomk4sumo.*
import org.scenariotools.iosmk.*

val vehicleSpeedsMaintainer = scenario("VehicleSpeedsMaintainer") {
    do {
        waitFor(SymbolicMessageEvent(SumoSimulation::vehiclesUpdated))
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        for (vehicle in sumoSimulation.vehicles.values){
            if (urgent(vehicle to vehicle send SumoVehicle::refreshedSpeed).result()!!)
                urgent(vehicle to vehicle send SumoVehicle::speedChanged param vehicle.speed)
        }

    } while (!sumoSimulation.closed)
}
