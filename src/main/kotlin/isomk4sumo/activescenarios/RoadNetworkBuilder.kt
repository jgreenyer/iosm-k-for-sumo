package isomk4sumo.activescenarios

import isomk4sumo.*
import isomk4sumo.getSumoSimulationFromLastMessageEvent
import isomk4sumo.urgent
import org.scenariotools.iosmk.*


val roadNetworkBuilder = scenario("RoadNetworkBuilder") {
    // wait for first time step
    waitFor(SymbolicMessageEvent(SumoSimulation::do_timestep))
    val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

    urgent(sumoSimulation sendSelf SumoSimulation::getJunctionIDList).result()?.let{
        for (id in it){
            println("Adding junction $id")
            val sumoJunction = SumoJunction(sumoSimulation,id)
            sumoSimulation.junctions.put(id, sumoJunction)
            urgent(sumoJunction sendSelf SumoJunction::retrievePosition)
        }
    }

    urgent(sumoSimulation sendSelf SumoSimulation::getEdgeIDList).result()?.let{
        for (id in it){
            println("Adding edge $id")
            sumoSimulation.edges.put(id, SumoEdge(sumoSimulation,id))
        }
    }

    urgent(sumoSimulation sendSelf SumoSimulation::getLaneIDList).result()?.let{
        for (id in it){
            urgent(sumoSimulation sendSelf SumoSimulation::getLaneEdgeID param id).result()?.let {
                println("Adding lane $id, which belongs to edge $it")
                sumoSimulation.lanes.put(id, SumoLane(sumoSimulation,id,sumoSimulation.edges.get(it)!!))
            }
        }
    }

    urgent(sumoSimulation sendSelf SumoSimulation::getTrafficLightIDList).result()?.let {
        for (id in it){
            println("Adding traffic light $id")
            val trafficLight = SumoTrafficLight(sumoSimulation,id)
            sumoSimulation.trafficLights.put(id, trafficLight)
        }
    }

    urgent(sumoSimulation sendSelf SumoSimulation::roadNetworkCreated)

}
