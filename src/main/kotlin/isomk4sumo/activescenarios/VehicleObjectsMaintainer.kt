package isomk4sumo.activescenarios

import isomk4sumo.*
import org.scenariotools.iosmk.*

val vehicleObjectsMaintainer = scenario("VehicleObjectsMaintainer") {

    do {
        waitFor(SymbolicMessageEvent(SumoSimulation::do_timestep))
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        urgent(sumoSimulation sendSelf SumoSimulation::getDepartedIDList).result()?.let {
            for (id in it){
                urgent(sumoSimulation sendSelf SumoSimulation::vehicleAppeared param id)
            }
        }

        urgent(sumoSimulation sendSelf SumoSimulation::getArrivedIDList).result()?.let {
            for (id in it){
                urgent(sumoSimulation sendSelf SumoSimulation::vehicleDisAppeared param id)
            }
        }


        urgent(sumoSimulation sendSelf SumoSimulation::vehiclesUpdated)
    } while (!sumoSimulation.closed)
}
