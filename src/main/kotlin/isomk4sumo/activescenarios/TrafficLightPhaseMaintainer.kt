package isomk4sumo.activescenarios

import isomk4sumo.*
import org.scenariotools.iosmk.*

val trafficLightPhaseMaintainer = scenario("TrafficLightPhaseMaintainer") {

    waitFor(SumoSimulation::roadNetworkCreated.symbolicEvent())
    val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

    // communicate the initial TL phases
    for (sumoTrafficLight in sumoSimulation.trafficLights.values.toTypedArray()){ //using .toTypedArray() in order to avoid ConcurrentModificationException
        urgent(sumoTrafficLight to sumoTrafficLight.trafficLightController send TrafficLightController::phaseChanged param sumoTrafficLight.currentPhase)
    }

    // continuously communicate the TL phase changes
    do {

        waitFor(SymbolicMessageEvent(SumoSimulation::do_timestep))

        for (sumoTrafficLight in sumoSimulation.trafficLights.values.toTypedArray()){ //using .toTypedArray() in order to avoid ConcurrentModificationException
            if (urgent(sumoTrafficLight sendSelf SumoTrafficLight::refreshPhase).result()!!)
                urgent(sumoTrafficLight to sumoTrafficLight.trafficLightController send TrafficLightController::phaseChanged param sumoTrafficLight.currentPhase)
        }

    } while (!sumoSimulation.closed)
}