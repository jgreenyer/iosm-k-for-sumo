package isomk4sumo.activescenarios

import isomk4sumo.*
import isomk4sumo.getSumoSimulationFromLastMessageEvent
import isomk4sumo.urgent
import org.scenariotools.iosmk.*

val personsEdgeMaintainer = scenario("PersonsEdgeMaintainer"){
    do {
        waitFor(SumoSimulation::personsUpdated.symbolicEvent())
        val sumoSimulation = getSumoSimulationFromLastMessageEvent()!!

        //for each edge check the persons on it, then check whether the edge has changed.

        for(sumoEdge in sumoSimulation.edges.values.toTypedArray()){
            urgent(sumoEdge sendSelf SumoEdge::getLastStepPersonIDs).result()?.let{
                for (personID in it){
                    val sumoPerson = sumoSimulation.persons.get(personID)!!
                    if (sumoEdge != sumoPerson.edge){
                        urgent(sumoPerson sendSelf SumoPerson::changeEdge param sumoEdge)
                    }
                }
            }
        }

    } while (!sumoSimulation.closed)
}