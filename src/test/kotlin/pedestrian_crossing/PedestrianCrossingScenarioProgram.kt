package pedestrian_crossing

import isomk4sumo.activescenarios.*
import isomk4sumo.createSumoScenarioProgram
import pedestrian_crossing.guarantees.activateTrafficLightOnPersonApproaching
import pedestrian_crossing.guarantees.carsMustHaveGreenFor15Sec


fun main(args: Array<String>) {
    val scenarioProgram = createSumoScenarioProgram("sumo-gui.exe",
            "src/test/resources/pedestrian_crossing/data/run.sumocfg",
            0.1)

    scenarioProgram.addAssumptionScenario(roadNetworkBuilder)
    scenarioProgram.addAssumptionScenario(vehicleObjectsMaintainer)
    scenarioProgram.addAssumptionScenario(vehicleLanesMaintainer)
    scenarioProgram.addAssumptionScenario(vehiclePositionsMaintainer)

    scenarioProgram.addAssumptionScenario(trafficLightPhaseMaintainer)
    //scenarioProgram.addAssumptionScenario(vehicleSpeedsMaintainer)

    scenarioProgram.addAssumptionScenario(personsEdgeMaintainer)
    scenarioProgram.addAssumptionScenario(personsMaintainer)

    scenarioProgram.addGuaranteeScenario(activateTrafficLightOnPersonApproaching)
    scenarioProgram.addGuaranteeScenario(carsMustHaveGreenFor15Sec)

    scenarioProgram.run()

}