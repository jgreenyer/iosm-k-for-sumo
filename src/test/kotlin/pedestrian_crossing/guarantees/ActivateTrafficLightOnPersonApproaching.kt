package pedestrian_crossing.guarantees

import isomk4sumo.SumoEdge
import isomk4sumo.SumoPerson
import isomk4sumo.SumoTrafficLight
import isomk4sumo.getSumoPersonFromLastMessageEvent
import org.scenariotools.iosmk.*

val activateTrafficLightOnPersonApproaching = scenario("TriggerPedestriansGreenUponPedestrianApproaching", SymbolicMessageEvent(SumoPerson::changeEdge, ANY)){

    val trafficLightTriggerAreaIDs = arrayOf(":C_w0", ":C_w1")
    val trafficLightID = "C"

    val minDelayBetweenGreenForPedestrians = 150 // steps of 0.1 sec = 45 sec.

    val sumoPerson = getSumoPersonFromLastMessageEvent()!!
    val sumoSimulation = sumoPerson.sumoSimulation

    val edge = lastMessage()!!.parameters[0] as SumoEdge

    if (edge.id in trafficLightTriggerAreaIDs){
        //println("Person ${person.id} in traffic light trigger area ${edge.id}")

        // if TL is green set to next TL phase (as described in pedcrossing.tll.xml)
        val sumoTrafficLight = sumoSimulation.trafficLights.get(trafficLightID)!!
        val currentPhase = sumoTrafficLight.currentPhase

        //println("### currentPhase of TL ${trafficLightID} is $currentPhase")

        if (currentPhase != TrafficLightPhases.pedestriansGreenPhase){
            request(sumoTrafficLight.trafficLightController to sumoTrafficLight send SumoTrafficLight::setPhase param TrafficLightPhases.vehicleYellowPhase)
        }else{
            //println("### already TL ${trafficLightID} activated for ${person.id}")
        }
    }

}

