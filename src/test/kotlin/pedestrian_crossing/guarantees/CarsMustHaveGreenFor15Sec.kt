package pedestrian_crossing.guarantees

import isomk4sumo.SumoSimulation
import isomk4sumo.SumoTrafficLight
import isomk4sumo.getSumoTrafficLightFromLastMessageEvent
import org.scenariotools.iosmk.*

val carsMustHaveGreenFor15Sec = scenario("VehiclesMustHaveGreenFor15Sec", SymbolicMessageEvent(SumoTrafficLight::refreshPhase)){

    val trafficLightID = "C"
    val minDelayBetweenGreenForPedestrians = 150 // steps of 0.1 sec = 45 sec.

    if (lastMessage()!!.result() as Boolean){ // TL phase did change...

        val sumoTrafficLight = getSumoTrafficLightFromLastMessageEvent()!!

        if (sumoTrafficLight.currentPhase == TrafficLightPhases.vehicleYellowPhase){ // ...and it changed to yellow

            // Blocking TL changed to yellow...
            blockedEvents.add(sumoTrafficLight.trafficLightController to sumoTrafficLight send SumoTrafficLight::setPhase param TrafficLightPhases.vehicleYellowPhase)

            // ... while it cycles back to showing green to vehicles...
            do{
                waitFor(sumoTrafficLight sendSelf SumoTrafficLight::refreshPhase)
            }while(sumoTrafficLight.currentPhase != TrafficLightPhases.vehicleGreenPhase)

            //println("+++ TL $trafficLightID turned GREEN for cars.")

            // ... and then a bit longer, to guarantee a certain minimum green time for cars.
            repeat(minDelayBetweenGreenForPedestrians) {
                //println("+++ Blocking TL ${trafficLightID}, must keep green for vehicles ${minDelayBetweenGreenForVehicles-it} more steps")
                waitFor(sumoTrafficLight.sumoSimulation sendSelf SumoSimulation::do_timestep)
            }
        }
    }
}

