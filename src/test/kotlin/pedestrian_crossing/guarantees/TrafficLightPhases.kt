package pedestrian_crossing.guarantees

object TrafficLightPhases{
    val vehicleGreenPhase = 0
    val vehicleYellowPhase = 1
    val pedestriansGreenPhase = 2
    val vehicleAndPedestriansRedPhase = 3
}