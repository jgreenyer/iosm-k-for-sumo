package pedestrian_crossing_car_to_x

import isomk4sumo.activescenarios.*
import isomk4sumo.createSumoScenarioProgram
import pedestrian_crossing_car_to_x.assumptions.carApproachesJunctionWithTL
import pedestrian_crossing_car_to_x.assumptions.carPassesJunctionWithTL
import pedestrian_crossing_car_to_x.assumptions.tlControllerDetectsAllPedestriansLeft
import pedestrian_crossing_car_to_x.assumptions.tlControllerDetectsPedestrian
import pedestrian_crossing_car_to_x.guarantees.cars.triggerVehiclesGreenUponVehicleRequest
import pedestrian_crossing_car_to_x.guarantees.cars.vehicleNotifiesTLControllerOnApproach
import pedestrian_crossing_car_to_x.guarantees.cars.vehiclesGreenMustOccurAfterVehicleRequest
import pedestrian_crossing_car_to_x.guarantees.cars.vehiclesMustHaveGreenFor15Sec
import pedestrian_crossing_car_to_x.guarantees.countTLPhaseActiveDuration
import pedestrian_crossing_car_to_x.guarantees.manageTLPhaseTransitioning
import pedestrian_crossing_car_to_x.guarantees.pedestrians.pedestriansGreenMustOccurAfterPedestrianApproaching
import pedestrian_crossing_car_to_x.guarantees.pedestrians.pedestriansMustHaveGreenFor15Sec
import pedestrian_crossing_car_to_x.guarantees.pedestrians.triggerPedestriansGreenUponPedestrianApproaching


fun main(args: Array<String>) {
    val sp = createSumoScenarioProgram("sumo-gui.exe",
            "src/test/resources/pedestrian_crossing_car_to_x/data/run.sumocfg",
            0.1)

    sp.addAssumptionScenario(roadNetworkBuilder)
    sp.addAssumptionScenario(vehicleObjectsMaintainer)
    sp.addAssumptionScenario(vehicleLanesMaintainer)
    sp.addAssumptionScenario(vehiclePositionsMaintainer)

    sp.addAssumptionScenario(trafficLightPhaseMaintainer)
    //scenarioProgram.addAssumptionScenario(vehicleSpeedsMaintainer)


    sp.addAssumptionScenario(personsEdgeMaintainer)
    sp.addAssumptionScenario(personsMaintainer)

    sp.addAssumptionScenario(carApproachesJunctionWithTL)
    sp.addAssumptionScenario(carPassesJunctionWithTL)
    sp.addAssumptionScenario(tlControllerDetectsPedestrian)
    sp.addAssumptionScenario(tlControllerDetectsAllPedestriansLeft)


    sp.addGuaranteeScenario(triggerVehiclesGreenUponVehicleRequest)
    sp.addGuaranteeScenario(vehicleNotifiesTLControllerOnApproach)
    sp.addGuaranteeScenario(vehiclesMustHaveGreenFor15Sec)
    sp.addGuaranteeScenario(vehiclesGreenMustOccurAfterVehicleRequest)

    sp.addGuaranteeScenario(pedestriansMustHaveGreenFor15Sec)
    sp.addGuaranteeScenario(triggerPedestriansGreenUponPedestrianApproaching)
    sp.addGuaranteeScenario(pedestriansGreenMustOccurAfterPedestrianApproaching)

    sp.addGuaranteeScenario(countTLPhaseActiveDuration)
    sp.addGuaranteeScenario(manageTLPhaseTransitioning)

    sp.run()

}
