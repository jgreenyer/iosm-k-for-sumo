package pedestrian_crossing_car_to_x.guarantees

object TrafficLightPhases{
    val vehicleGreenPhase = 0
    val vehicleYellowPhase = 1
    val pedestriansGreenPhase = 2
    val vehicleAndPedestriansRedPhase = 3
}