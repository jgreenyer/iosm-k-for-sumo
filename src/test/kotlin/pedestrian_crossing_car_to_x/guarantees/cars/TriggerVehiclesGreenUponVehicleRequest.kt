package pedestrian_crossing_car_to_x.guarantees.cars

import isomk4sumo.TrafficLightController
import isomk4sumo.VehicleController
import isomk4sumo.getTrafficLightControllerFromLastMessageEvent
import isomk4sumo.getVehicleControllerFromLastMessageEvent
import org.scenariotools.iosmk.*
import pedestrian_crossing_car_to_x.guarantees.TrafficLightPhases

val triggerVehiclesGreenUponVehicleRequest = scenario("TriggerVehiclesGreenUponVehicleRequest", TrafficLightController::vehicleApproaching.symbolicEvent()){

    val vehicleController = getVehicleControllerFromLastMessageEvent()!!
    val trafficLightController = getTrafficLightControllerFromLastMessageEvent()!!

    val vehiclePassedTLEvent = ANY to vehicleController sendSymbolic VehicleController::passedTrafficLight param trafficLightController

    interruptingEvents.add(vehiclePassedTLEvent)

    if (trafficLightController.sumoTrafficLight.currentPhase != TrafficLightPhases.vehicleGreenPhase) {
        // if not green, then request green
        println("*** requesting green for vehicles...")
        request(trafficLightController sendSelf TrafficLightController::transitionToPhase param TrafficLightPhases.vehicleGreenPhase)
        println("*** requestED green for vehicles...")
    }else{
        // if green for vehicles for less than 15 seconds, keep green for cars, otherwise allow other transitionToPhase requests.
        if (trafficLightController.currentPhaseActiveSeconds <= 15){
            interruptingEvents.add(trafficLightController sendSelfSymbolic TrafficLightController::currentPhasedIsActiveFor param 15)

            println("***** keep green for vehicle ${vehicleController.sumoVehicle.id}...")
            blockedEvents.add(trafficLightController sendSelfSymbolic TrafficLightController::transitionToPhase)
            waitFor(vehiclePassedTLEvent)

        }
    }

}
