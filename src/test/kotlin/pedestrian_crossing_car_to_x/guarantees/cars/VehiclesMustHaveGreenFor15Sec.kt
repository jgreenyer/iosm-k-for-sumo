package pedestrian_crossing_car_to_x.guarantees.cars

import isomk4sumo.TrafficLightController
import isomk4sumo.getTrafficLightControllerFromLastMessageEvent
import isomk4sumo.waitForSimulationSteps
import org.scenariotools.iosmk.param
import org.scenariotools.iosmk.scenario
import org.scenariotools.iosmk.sendSelfSymbolic
import org.scenariotools.iosmk.symbolicEvent
import pedestrian_crossing_car_to_x.guarantees.TrafficLightPhases

val vehiclesMustHaveGreenFor15Sec = scenario ("VehiclesMustHaveGreenFor15Sec",
        TrafficLightController::phaseChanged.symbolicEvent() param TrafficLightPhases.vehicleGreenPhase){

    val minDelayBetweenGreenForVehicles = 150 // steps of 0.1 sec = 15 sec.

    val trafficLightController = getTrafficLightControllerFromLastMessageEvent()!!

    blockedEvents.add(trafficLightController sendSelfSymbolic TrafficLightController::transitionToPhase)

    waitForSimulationSteps(minDelayBetweenGreenForVehicles)

}

