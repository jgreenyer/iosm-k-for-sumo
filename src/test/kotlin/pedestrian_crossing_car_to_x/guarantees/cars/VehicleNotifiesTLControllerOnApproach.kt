package pedestrian_crossing_car_to_x.guarantees.cars

import isomk4sumo.TrafficLightController
import isomk4sumo.VehicleController
import isomk4sumo.getVehicleControllerFromLastMessageEvent
import isomk4sumo.urgent
import org.scenariotools.iosmk.scenario
import org.scenariotools.iosmk.send
import org.scenariotools.iosmk.symbolicEvent

val vehicleNotifiesTLControllerOnApproach = scenario("VehicleNotifiesTLControllerOnApproach", VehicleController::inTrafficLightPerimeter.symbolicEvent()){

    val vehicleController = getVehicleControllerFromLastMessageEvent()!!
    val trafficLightController = lastMessage()!!.parameters[0] as TrafficLightController

    urgent(vehicleController to trafficLightController send TrafficLightController::vehicleApproaching)

}
