package pedestrian_crossing_car_to_x.guarantees.pedestrians

import isomk4sumo.SumoTrafficLight
import isomk4sumo.TrafficLightController
import org.scenariotools.iosmk.*
import pedestrian_crossing_car_to_x.guarantees.TrafficLightPhases

val triggerPedestriansGreenUponPedestrianApproaching =
scenario("TriggerPedestriansGreenUponPedestrianApproaching",
        TrafficLightController::pedestrianApproachingCrossing.symbolicEvent()){

    val trafficLightController = lastMessage()!!.receiver as TrafficLightController
    val sumoTrafficLight = lastMessage()!!.sender as SumoTrafficLight
    interruptingEvents.add(sumoTrafficLight to trafficLightController send TrafficLightController::allPedestriansLeftTriggerAreas)
    println("### requesting green for pedestrians")
    request(trafficLightController sendSelf TrafficLightController::transitionToPhase param TrafficLightPhases.pedestriansGreenPhase)
    println("### requestED green for pedestrians")
}

