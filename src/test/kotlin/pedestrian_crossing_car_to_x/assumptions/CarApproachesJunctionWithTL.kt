package pedestrian_crossing_car_to_x.assumptions

import isomk4sumo.*
import org.scenariotools.iosmk.*

val carApproachesJunctionWithTL = scenario("CarApproachesJunctionWithTL", SumoVehicle::positionChanged.symbolicEvent()){

    val PERIMITER = 90

    val vehiclePos1 = (lastMessage()!!.parameters[0] as Position2D).copy()
    val sumoVehicle = getSumoVehicleFromLastMessageEvent()!!

    waitFor(sumoVehicle to sumoVehicle sendSymbolic SumoVehicle::positionChanged param ANY)

    val vehiclePos2 = (lastMessage()!!.parameters[0] as Position2D).copy()

    val sumoSimulation = sumoVehicle.sumoSimulation

    for (sumoJunctionEntry in sumoSimulation.junctions){
        val trafficLightAtJunction = sumoSimulation.trafficLights.get(sumoJunctionEntry.key)
        trafficLightAtJunction?.let {
            val junctionPosition = sumoJunctionEntry.value.position
            if(junctionPosition.distance(vehiclePos1) > PERIMITER &&
                    junctionPosition.distance(vehiclePos2) <= PERIMITER ) {
                urgent(sumoSimulation.environment
                                to sumoVehicle.vehicleController
                                send VehicleController::inTrafficLightPerimeter
                                param trafficLightAtJunction.trafficLightController)
            }
        }
    }

}

