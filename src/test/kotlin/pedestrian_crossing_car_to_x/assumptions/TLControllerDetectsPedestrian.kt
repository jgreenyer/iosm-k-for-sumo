package pedestrian_crossing_car_to_x.assumptions

import isomk4sumo.*
import org.scenariotools.iosmk.*

val tlControllerDetectsPedestrian = scenario("VehicleNotifiesTLControllerOnApproach", SumoPerson::changeEdge.symbolicEvent() param ANY){

    val sumoPerson = getSumoPersonFromLastMessageEvent()!!
    val sumoSimulation = sumoPerson.sumoSimulation

    val edge = lastMessage()!!.parameters[0] as SumoEdge
    //val person = lastMessage()!!.receiver as SumoPerson

    if (edge.id.startsWith(":") && edge.id.contains("w")){

        val trafficLightID = edge.id.substringBefore("_").removePrefix(":")

        sumoSimulation.trafficLights.get(trafficLightID)?.let {
            val sumoTrafficLight = it
            val trafficLightController = sumoTrafficLight.trafficLightController

            //println("### pedestrian detected")

            if (trafficLightController.detectedPedestrianIDs.contains(sumoPerson.id)){
                //println("### pedestrian leaving the crossing")
                urgent(trafficLightController sendSelf TrafficLightController::removeDetectedPedestrianIDs param sumoPerson.id)
            }else{
                //println("### pedestrian approaching the crossing")
                urgent(trafficLightController sendSelf TrafficLightController::addDetectedPedestrianIDs param sumoPerson.id)
                urgent(sumoTrafficLight to trafficLightController send  TrafficLightController::pedestrianApproachingCrossing param sumoPerson.id)
            }
        }

    }

}

